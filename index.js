// index.js
require('./src/data/db');
const botData = require('./src/data/bot');
const express = require("express");

const app = express();
const port = process.env.PORT || "8000";

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  next();
});

app.get("/status", (req, res) => {
  botData.findAll(function(err, bots){
    if(err){
      res.status(500).send("Oops, something wrong");
    } else {
      res.status(200).send(bots.map(function(b) {
        return {name:b.name, status: b.status};
      }));
    }
  });
  
});

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});