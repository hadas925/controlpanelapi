const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
 
const Bot = new Schema({
    _id: ObjectId,
    name: String,
    status: String,
    contactDetails: String,
    expertise: [String],
    answer: String
});

const BotModel = mongoose.model('bots', Bot, 'bots_copy');

module.exports = {
    findAll:function(callback){
        BotModel.find({}, function (err, bots) {
            callback(err, bots);
        });
    }
};
